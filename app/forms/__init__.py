from flask.ext.wtf import Form
from wtforms.fields import StringField, SubmitField
from wtforms.validators import DataRequired


class RedirectForm(Form):
    url = StringField('URL', validators=[DataRequired('Required')])
    submit = SubmitField('Go')
