import pycurl
from flask import render_template
from io import BytesIO
from . import home
from ..forms import RedirectForm

headers = {}
buffer = BytesIO()
b = ''


@home.route('/301notnier/index', methods=['GET', 'POST'])
def index():
    global buffer
    buffer = BytesIO()
    h=''
    form = RedirectForm()
    if form.validate_on_submit():
        get_headers(form.url.data, buffer)
        return render_template('home/index.html', form=form, headers=headers, h=b.replace('\r\n', '<br/>'))
    return render_template('home/index.html', form=form)


def get_headers(url, buffer=None):
    global b
    b = ''
    try:
        curl = pycurl.Curl()
        curl.setopt(pycurl.URL, url)
        curl.setopt(pycurl.FOLLOWLOCATION, True)
        curl.setopt(pycurl.AUTOREFERER, True)
        curl.setopt(pycurl.HEADER, True)
        curl.setopt(pycurl.NOBODY, True)
        curl.setopt(pycurl.WRITEFUNCTION, buffer.write)
        curl.setopt(pycurl.HEADERFUNCTION, header_function)
        curl.setopt(pycurl.USERAGENT, 'Mozilla/5.0 (compatible; pycurl)')
        curl.perform()
        b = buffer.getvalue().decode('utf-8')
        # curl.getinfo()
        curl.close()
    except:
        pass


def header_function(header_line):
    # HTTP standard specifies that headers are encoded in iso-8859-1.
    # On Python 2, decoding step can be skipped.
    # On Python 3, decoding step is required.
    header_line = header_line.decode('iso-8859-1')

    # Header lines include the first status line (HTTP/1.x ...).
    # We are going to ignore all lines that don't have a colon in them.
    # This will botch headers that are split on multiple lines...
    if ':' not in header_line:
        return

    # Break the header line into header name and value.
    name, value = header_line.split(':', 1)

    # Remove whitespace that may be present.
    # Header lines include the trailing newline, and there may be whitespace
    # around the colon.
    name = name.strip()
    value = value.strip()

    # Header names are case insensitive.
    # Lowercase name here.
    name = name.lower()

    # Now we can actually record the header name and value.
    headers[name] = value
