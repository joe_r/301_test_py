import datetime

from flask.ext.login import UserMixin
from flask.ext.security import RoleMixin
from . import db, bcrypt


class Role(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)


class User(db.Document, UserMixin):
    email = db.EmailField(max_length=255, unique=True)
    _password = db.StringField(max_length=255)
    active = db.BooleanField(default=True)
    confirmed_at = db.DateTimeField(default=datetime.now())
    roles = db.ListField(db.ReferenceField(Role), default=[])

    @property
    def password(self):
        return self._password

    @password.setter
    def _set_password(self, plaintext):
        self._password = bcrypt.generate_password_hash(plaintext)

