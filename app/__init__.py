# import sys
# sys.path.insert(0, '/Users/jrienton/.virtualenvs/utils-py/lib/python3.5/site-packages')

from flask import Flask
from flask_bootstrap import Bootstrap
from .home import home as home_blueprint

app = Flask(__name__, instance_relative_config=True)
app.config.from_object('config.default')
app.config.from_pyfile('config.py', silent=True)

bootstrap = Bootstrap(app)

# Load blueprints (if any)
app.register_blueprint(home_blueprint)
