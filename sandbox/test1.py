import pycurl
from io import BytesIO
from pprint import pprint

buffer = BytesIO()
c = pycurl.Curl()
c.setopt(c.URL, 'http://inpnetwork.com/')
c.setopt(c.WRITEDATA, buffer)
c.setopt(pycurl.FOLLOWLOCATION, True)
c.setopt(pycurl.HEADER, True)
c.setopt(pycurl.NOBODY, True)
c.perform()
# print(c.getinfo(pycurl.RESPONSE_CODE))
c.close()

body = buffer.getvalue()
pprint(body.decode('utf-8').replace('\r\n','\n'))
# Body is a string in some encoding.
# In Python 2, we can print it without knowing what the encoding is.
